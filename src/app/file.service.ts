import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpResponse } from '@angular/common/http';

@Injectable()
export class FileService {
  constructor(private http: HttpClient) {}

  getData(): Promise<object> {
    const httpOptins = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.http
      .get(
        'http://api.openweathermap.org/data/2.5/weather?q=Hyderabad&appid=0bc803e79bc9bbdf6bb6ebcf6f9463c8',
        {}
      )
      .toPromise();
  }
  
  getPastData():Promise<object> {
    const httpOptins2 = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.http
      .get(
        'https://api.openweathermap.org/data/2.5/forecast?id=524901&appid=0bc803e79bc9bbdf6bb6ebcf6f9463c8',
        {}
      )
      .toPromise();
  }

getFutureData(): Promise<object> {
  const httpOptins = new HttpHeaders({ 'Content-Type': 'application/json' });

  return this.http
    .get(
      'http://api.openweathermap.org/data/2.5/weather?q=Hyderabad&appid=0bc803e79bc9bbdf6bb6ebcf6f9463c8',
      {}
    )
    .toPromise();
}
}

