import { Component, OnInit } from '@angular/core';
import { FileService } from '../file.service';
// export interface WeatherData{
//   temp:number,
//   temp_max:number,
//   temp_min:number,
//   speed:number,
//   deg:number
// }
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  showTable: Boolean = false;
  tableArray:Array<any> = [];
  weatherData: any;
  //past
  showTable2: Boolean = false;
  tableArray2: any = [];
  pastweatherData:any;
  //future
  showTable3: Boolean = false;
  tableArray3: any = [];
  futureweatherData: any;

  constructor(private service: FileService) {}

  ngOnInit() {}
  getWeather() {
    this.service.getData().then(x => {
      // const weatherData:object = x;
      this.weatherData = x;
      this.showTable = true;
      const data = this.tableArray.push(this.weatherData.main);
      console.log(this.tableArray);
    });
  }
  getPastWeather() {
    this.service.getPastData().then(y => {
      this.pastweatherData = y;
      this.showTable2 = true;
      const data2 = this.tableArray2.push(this.pastweatherData.list[1].main);
      console.log(this.tableArray2);
    });
  }
  getFutureWeather() {
    this.service.getFutureData().then(z => {
      this.futureweatherData = z;
      this.showTable3 = true;
      const data3 = this.tableArray3.push(this.futureweatherData.wind);
      console.log(this.tableArray3);
    });
  }
}


