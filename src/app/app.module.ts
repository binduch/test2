import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FileService } from './file.service'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers:[FileService],
  bootstrap: [AppComponent]
})
export class AppModule { }
